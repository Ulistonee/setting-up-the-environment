import React from "react";
import './slider.css'

const Index = () => {
  let isToggleOn = true;
  let body = document.querySelector('body');

  function changeTheme(){
    if (isToggleOn){
      body.classList.add('dark');
    }
    else {
      body.classList.remove('dark');
    }
    isToggleOn = !isToggleOn;
  }

  return (
    <label className="switch" htmlFor="checkbox">
      <input type="checkbox" id="checkbox" />
      <div className="slider round" onClick={changeTheme}></div>
    </label>
  )
}

export default Index;
