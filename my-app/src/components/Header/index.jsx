import React from "react";
import "./Header.css"

const Index = ({ title }) => {
  return (
    <h1 className="header">{title}</h1>
  )
}

export default Index;
