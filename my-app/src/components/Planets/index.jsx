import React from "react";
import './Planets.css'


const Index = ({ planets }) => {
  return (
    <ul className="planets-list">
      {planets.map((planet) => {
        return <li key={planet}>{planet}</li>
      })}
    </ul>
  )
}

export default Index;
