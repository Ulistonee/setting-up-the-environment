import './App.css';
import React from "react";
import Header from "./components/Header";
import Planets from "./components/Planets";
import Slider from "./components/Slider";

const planets = ["Mercury",
  "Venus",
  "Earth",
  "Mars",
  "Jupiter",
  "Saturn",
  "Uranus",
  "Neptune"]


function App() {
  return (
    <>
      <Header title="Solar system planets" />
      <Planets planets={planets}/>
      <Slider />
    </>
);
}

export default App;
